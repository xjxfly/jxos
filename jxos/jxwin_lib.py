__all__ = [
		'get_api_list',
		'empty_clipboard',
		'get_all_handle_arr',
		'get_handle_arr',
		'get_process_id',
		'get_window_attrs',
		'get_mydocument_dir',
		'goto_window',
		'is_my_handle',
		'kill',
		'start',
		'xprint',
		'restore',
		]



def get_api_list():
	"""
	功能说明：本函数仅仅用于返回可用接口列表，不在列表中列出的函数或接口等请不要调用，他们一般在内部实现使用，未来可能会改名或调整！！

	Returns
	-------
	返回值：可用接口, list形式

	"""
	return __all__

			
#coding=utf-8

"""
作者：xjxfly
邮箱：xjxfly@qq.com

说明：
这是 for windows 的通用自定义函数形成的基础库
本模块中出现的 arr 字样，通常是指 list

"""

import time
import re
import os
import ctypes
import signal
#import pkg_resources
#import tempfile

import win32api
import win32gui
import win32ui
import win32print
import win32process
import win32con
import win32clipboard

import pywinauto
from pywinauto import application

#import pyautogui

import jxbase as jxb 					# 引入自定义通用基础库


# --------------

#from . import common_config as cf 		# 引入自定义常量








#########=====================================================




def empty_clipboard():
	"""
	功能说明：本接口对剪贴板做一次清空操作

	Returns
	-------
	None.

	"""
	win32clipboard.OpenClipboard()
	win32clipboard.EmptyClipboard()
	win32clipboard.CloseClipboard()	
	
	
	






def get_all_handle_arr():
	"""
	说明：该函数通过调用 win32gui.EnumWindows() 获取所有的窗口句柄以 list 形式返回
	返回值：所有窗口句柄组成的 list
	"""
	all_handle_arr = []
	# 下面这个函数解释起来费劲，EnumWindows() 函数必须两个参数，前一个是用户定义的回调函数，
	# 这个回调函数的第一个参数 hWnd 位置是固定的，由系统返回的顶层窗口句柄，
	# 第2个参数 param 接受 EnumWindows() 的第 2 个参数作用它的参数，然后将所有句柄返回到第2个参数中，即此处的 h_arr。
	win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), all_handle_arr) 		

	return all_handle_arr









def get_handle_arr(class_name=None, class_name_re=None, parent=None, process=None, title=None, title_re=None, top_level_only=True, visible_only=True, enabled_only=False, best_match=None, handle=None, ctrl_index=None, found_index=None, predicate_func=None, active_only=False, control_id=None, control_type=None, auto_id=None, framework_id=None, backend=None, depth=None):
	"""
	功能说明：根据传入的参数，调用 pywinauto.findwindows.find_windows() 功能来返回相应的句柄构成的 list
		因为 pywinauto 的调用句子太长子，本接口只是对其做一层封装，其他不做任何改变。
		各参数的具体含义参见这里：https://pywinauto.readthedocs.io/en/latest/code/pywinauto.findwindows.html
		注意：这里的参数是完全按照 pywinauto 的 find_windows() 函数来设计的，所以参数的含义和 pywinauto 的 find_windows() 函数完全一致!!
		上面参数的默认值全部来自 pywinauto 的 find_windows() 函数，要注意到 top_level_only 的默认值是 True，而 visible_only 的默认值也是 True，也就是默认情况下，只返回可见的顶层窗口的句柄集！！！

	参数：
	----------
	Possible values are:

		class_name: 		Elements with this window class
		class_name_re: 		Elements whose class matches this regular expression
		parent: 			Elements that are children of this
		process: 			Elements running in this process
		title: 				Elements with this text
		title_re: 			Elements whose text matches this regular expression
		top_level_only: 	Top level elements only (default=**True**)
		visible_only: 		Visible elements only (default=**True**)
		enabled_only: 		Enabled elements only (default=False)
		best_match: 		Elements with a title similar to this
		handle: 			The handle of the element to return
		ctrl_index: 		The index of the child element to return
		found_index: 		The index of the filtered out child element to return
		predicate_func: 	A user provided hook for a custom element validation
		active_only: 		Active elements only (default=False)
		control_id: 		Elements with this control id
		control_type: 		Elements with this control type (string; for UIAutomation elements)
		auto_id: 			Elements with this automation id (for UIAutomation elements)
		framework_id: 		Elements with this framework id (for UIAutomation elements)
		backend: 			Back-end name to use while searching (default=None means current active backend)	

	返回值
	-------
		返回值：句柄构成的 list.
	"""

	handle_arr = get_handle_arr_by_pywinauto_findwindows(class_name=class_name, class_name_re=class_name_re, parent=parent, process=process, title=title, title_re=title_re, top_level_only=top_level_only, visible_only=visible_only, enabled_only=enabled_only, best_match=best_match, handle=handle, ctrl_index=ctrl_index, found_index=found_index, predicate_func=predicate_func, active_only=active_only, control_id=control_id, control_type=control_type, auto_id=auto_id, framework_id=framework_id, backend=backend, depth=depth)

	return handle_arr







	
	

def get_process_id(handle):	
	"""
	功能说明：根据句柄号，返回进程号
	Parameters
	----------
		handle : type: int
			说明：这是句柄号
	Returns
	-------
	返回值：进程号, int 类型

	"""
	thread_id, process_id = win32process.GetWindowThreadProcessId(handle)
	
	return process_id	









def get_window_attrs(handle):
	"""
	说明：根据传入的窗口句柄，返回该窗口的标题，类名等属性形成的 list 
	"""
	title = win32gui.GetWindowText(handle) 			# 根据句柄，获取窗口标题
	class_name = win32gui.GetClassName(handle) 		# 根据句柄，获取窗口类名
	# 构建一个 list返回，该 list 第1个元素是句柄所指的窗口的标题，第2个元素为句柄所指的窗口的类名
	attrs_arr = [title, class_name]

	return attrs_arr






	

def get_mydocument_dir():
	"""
	功能说明：获取 windows 系统提供的“我的文档”路径
	参数：无
	返回值：“我的文档”所在路径
	"""
	mydocument_path = None
	dll = ctypes.windll.shell32
	buf = ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH  + 1)
	if dll.SHGetSpecialFolderPathW(None, buf, 0x0005, False):
		#mydocument_path =  buf.value + '\\'
		#mydocument_path = jxb.change_path_style(xpath=mydocument_path, style='UNIX')
		mydocument_path = jxb.to_path(xpath=buf.value)

	return mydocument_path






# 这个 goto_window() 函数是旧版的，留在这里参考备用，新版的 goto_window() 函数在下面。
# def goto_window(app, title_re, class_name_re='', retry_count=5):
# 	"""
# 	功能说明：该函数用于返回到程序为 app对象（由 pywinauto 创建） 的 title_re 窗口上，并强烈清除该窗口下弹出的所有子孙窗口
# 		调用后将带致指定 app对象（由 pywinauto 创建）下的 标题为 title_re且类名为 class_name_re 的窗口，并该窗口上方的杂七杂八各种弹出窗口都关掉，只留主窗口
# 	参数：
# 		app: 程序运行后指向进程的对象
# 		title_re: 窗口标题，可以是普通字符串，也可以是正则形式
# 		class_name_re: 窗口类名，可以是普通字符串，也可以是正则形
# 		retry_count: 尝试次数，默认 5 次
# 	返回值：
# 		若通过层层清除能找到 app 所指的窗口标题包含 title_re 且类名为 class_name_re 的话，返回 True，否则返回 False
		
# 	"""
# 	func_name = jxb.get_current_function_name() + ': '

# 	if app is None:
# 		print(func_name + '目标应用程 app 还没启动，请先调用 start() 启动目标程序。')
# 		return False

# 	# 根据窗口标题来清除其上所有窗口, _ 表示不关心所取得的内容
# 	for _ in range(retry_count):
# 		#handle = app.top_window().handle
# 		#restore(handles=handle)			# 激活 app 所指的顶层窗口
# 		# 取得程序顶层窗口（即最上面一层，top window）的标题。以下两种方式都是正确的，选一种即可
# 		# top_window_title = app.top_window().texts()[0] 					
# 		top_window_title = app.top_window().window_text()
# 		top_window_class_name = app.top_window().class_name()
# 		# 传入的参数 title 是包含目标题的正则表达式，相当于 pattern，
# 		# 用 re.findall() 在当前窗口的标题 top_window_title 中去搜索正则模式的目标标题 title，
# 		# 如果没搜到，返回空的 list，表示当前窗口不是要找的窗口，关掉它。
# 		if len(re.findall(title_re, top_window_title)) > 0 and len(re.findall(class_name_re, top_window_class_name)) > 0: 
# 			return True
# 		else:				
# 			try:
# 				app.top_window().close()
# 			except:
# 				app.top_window().set_focus() 		# 把焦点落在顶层子窗口上
# 				app.top_window().close_alt_f4() 	# 用 ALT + F4 的方法关闭顶层窗口
# 			time.sleep(0.05)	
# 			continue					

# 	return False






def goto_window(app, title_re=None, class_name_re=None, retry_count=10):
	"""
	功能说明：该函数用于返回到程序为 app对象（由 pywinauto 创建） 的 title_re 窗口上，并强烈清除该窗口下弹出的所有子孙窗口
		调用后将带致指定 app对象（由 pywinauto 创建）下的 标题为 title_re且类名为 class_name_re 的窗口，并该窗口上方的杂七杂八各种弹出窗口都关掉，只留主窗口
	参数：
		app: 程序运行后指向进程的对象
		title_re: 窗口标题，可以是普通字符串，也可以是正则形式
		class_name_re: 窗口类名，可以是普通字符串，也可以是正则形式
		retry_count: 尝试次数，默认 5 次
	返回值：
		若通过层层清除能找到 app 所指的窗口标题包含 title_re 且类名为 class_name_re 的话，返回 True，否则返回 False
		
	"""
	func_name = jxb.get_current_function_name()

	if app is None:
		print(f'{func_name}: 错误：目标应用程 app 还没启动，请先调用 start() 启动目标程序。')
		return False
	
	if title_re is None and class_name_re is None:
		print(f'{func_name}: 错误：形参 title_re 和 class_name_re 不能都为None，请至少传入一个，字符串或正则表达式均可。')
		return False

	for _ in range(retry_count):
		top_window = app.top_window()
		top_window_title = top_window.window_text()
		top_window_class_name = top_window.class_name()

		title_match = title_re is None or re.findall(title_re, top_window_title)
		class_match = class_name_re is None or re.findall(class_name_re, top_window_class_name)

		if title_match and class_match:
			return True
		else:
			try:
				top_window.close()
			except:
				top_window.set_focus()
				top_window.close_alt_f4()
			time.sleep(0.05)

	return False







def is_my_handle(handle, title):
	"""
	说明：该函数根据输入的窗口句柄 handle 判断该窗口是否包含窗口标题 title，如果是，返回 True, 否则返回 False
	"""

	attrs_arr = get_window_attrs(handle=handle)
	handle_title = attrs_arr[0] 				# 句柄所指向的窗口的标题
	#if title.find(title) == -1: 				# 在返回的窗口标题中搜索是否包含想要找的标题字符串，若不是返回 -1。由于 title 是正则表达式，而 str.find() 不支持正则，所以用下面这句
	# 下面的 if 表示在句柄所指向的窗口的标题 handle_title 中搜索参数所指标题 title，若找到，返回 True，否则返回 False
	if len(re.findall(title, handle_title)) == 0:
		return False
	else:
		return True






def kill(process_id=None, app_name=None):
	"""
	功能说明：该任务函数的作用是用来杀掉应用程序
	参数：app_name 指向程序名；process_id 指向进程 id；这两个参数只需传入一个即可。
		若两个都传入，只优先使用 process_id，若采用 process_id 关闭失败，则再采用 app_name 来关闭。
	返回值：灭杀应用程序或进程成功的话返回 True，否则返回 False
	"""
	func = jxb.get_current_function_name()
	
	if process_id is None and app_name is None:
		print(func,' 请传入程序文件名给形参 app_name 或传入进程id 给形参 process_id')
		return False

	result = False
	
	if not result:
		if process_id is not None:
			result = kill_app_by_process_id(process_id=process_id)

	if not result:
		if app_name is not None:
			result = kill_app_by_name(app_name=app_name)

	return result








def start(app_path, retry_count=5, wait_time=10, by='pywinauto'):
	"""
	功能说明：本接口调用参数 by 指定的功能来启动 app_path 所指的程序
	Parameters
	----------
		app_path : type: str. 要启动的程序的全路径（含程序名）
		retry_count : TYPE, int，optional。启动失败后尝试的次数，default: 5次
		wait_time : TYPE: int, optional。程序启动成功后等待几秒，default: 10 秒（这是第一次启动要等待的时间）
		by : TYPE：str, optional, default: 'pywinauto': 表示调用什么接口来启动程序，默认调用 pywinauto 来启动。
	Returns
	-------
	返回值：返回程序启动成功后的指向程序的对象，若失败，则返回 None

	"""
	app = None
	if by.upper() == 'PYWINAUTO':
		app = start_app_by_pywinauto(app_path=app_path, retry_count=retry_count, wait_time=wait_time)
		
	return app










def xprint(data_arr):
	"""
	功能说明：对传入的二维数据list data_arr，发往系统默认打印机进行打印
	参数：
		data_arr : 是个二维 list, 每个元素是一维 list，每个一维 list 包含3个元素，分别是待打印的数据，横坐标（单位：像素），纵坐标（单位：像素）
	返回值：
		True: 打印成功
		False: 无法打印

	"""
	# X from the left margin, Y from top margin
	# both in pixels
	# 定义打印起始位置（单位为像素）
	if isinstance(data_arr, list) and len(data_arr) > 0:
		# ------------
		# 初始化打印机
		printer_name = win32print.GetDefaultPrinter()
		hDC = win32ui.CreateDC()
		hDC.CreatePrinterDC(printer_name)
		# 显示到打印队列的名称
		hDC.StartDoc('python print')
		hDC.StartPage()
		# ---------------------
		# 准备要打印的数据
		for arr in data_arr: 
			if len(arr) not in [1,3]:
				print('data_arr 中的每一个元素必须是一个一维 list, 且这个一维 list 要么只有一个数据元素，要么只有3个元素，其中第一个是数据，后两个是横纵坐标值（单位：像素）')
				continue
			
			data_str = str(arr[0]) 		# 第0个元素为数据内容
			if len(arr) == 1:
				X = 50 		# 横坐标（单位：像素）
				Y = 50 		# 纵坐标（单位：像素）
			if len(arr) == 3:
				X = int(arr[1]) 	# 横坐标（单位：像素）
				Y = int(arr[2]) 	# 纵坐标（单位：像素）
				
			# 转换待打印的数据内容到 list
			sub_data_arr = str(data_str).split('\n')
			# multi_line_string = input_string.split()
			# 获取默认打印机
			for line in sub_data_arr:
				hDC.TextOut(X, Y, line)
				Y += 100
		# ----------------------
		# 发送打印数据到打印机
		# hDC.TextOut(X,Y,str(data)) 
		hDC.EndPage()
		# 下面 EndDoc() 指令发送后才真正开始打印
		hDC.EndDoc() 		# EndDoc() 为发送打印命令，这句执行后才打印机才真正开始打印！！！！！
		hDC.DeleteDC() 		# 删除打印任务
		return True
	else:
		print('传入的 data_arr 必须是个二维 list, 其中每一个元素也必须是一维 list，这个一维 list 必须包含3个元素，分别是数据（字符串），横坐标值（像素），纵坐标值（像素）')
		return False









def restore(handles):
	"""
	功能说明：根据传入的窗口句柄（可以单个句柄，也可以是一堆句柄放在 list 中传进来），
		调用 windows 系统功能去恢复窗口并激活它，让他成为活动窗口，以便可以交互.
		注意：本接口在许多场用得到，非常重要！！！ 特别是其他许多恢复窗口的办法无效的情况下，本接口可能依然能恢复窗口，
		因为他是通过 windows  api 系统调用去呼叫应用程序窗口的，应用程序可以忽略其他同行，但对来自 OS 的呼叫一般不会忽略。
	参数：
		handles: 待恢复并激活的窗口句柄群
	Returns
	-------
	返回值：无

	"""
	if not isinstance(handles, list):
		handles = [handles]
	
	# 记录已经激活的句柄
	activated_handles = []
	for handle in handles:
		try:
			# 下面这个 SendMessage() 是阻塞的，不利于自动化，所以注释掉，用后面的 PostMessage()，它是不阻塞的，发完消息立马往下走。
			# SendMessage() 和 PostMessage() 除了向 handle 所指窗口发消息外，还有一项额外的重要功能，就是让 handle 所指的窗口（无论是否最小化）恢复到正常大小，
			# 本接口主要就是为了通过系统调用向 handle 所指的窗口发送消息使窗口恢复的功能，以便 UI 自动化能进行下去。
			# win32gui.ShowWindow(handle, win32con.SW_RESTORE)
			#win32gui.SendMessage(handle, win32con.WM_SYSCOMMAND, win32con.SC_RESTORE, 0)
			win32gui.PostMessage(handle, win32con.WM_SYSCOMMAND, win32con.SC_RESTORE, 0)
			result = win32gui.SetForegroundWindow(handle) 		# 把焦点设到句柄所指窗口，即，使其成为活动窗口
			if result:
				activated_handles.append(handle)
			else:
				# print(f'警告：无法激活句柄 {handle} 所指的窗口。') 		# 如果无法激活，则跳过，不记录，因为输出
				pass
		except Exception as e:
			print(f'错误：激活句柄 {handle} 所指的窗口失败。错误信息：{e}')
		
	return activated_handles
				

				
	
	
	
	
	
	
	
def restore_backup(handle):
	"""
	功能说明：根据传入的窗口句柄，调用 windows 系统功能去恢复窗口并激活它，让他成为活动窗口，以便可以交互.
		注意：本接口在许多场用得到，非常重要！！！ 特别是其他许多恢复窗口的办法无效的情况下，本接口可能依然能恢复窗口，
		因为他是通过 windows  api 系统调用去呼叫应用程序窗口的，应用程序可以忽略其他同行，但对来自 OS 的呼叫一般不会忽略。
	参数：
		handle: 待恢复并激活的窗口句柄
	Returns
	-------
	返回值：True or False, 表示恢复及激活成功还是失败

	"""
	try:
		hForeWnd = win32gui.GetForegroundWindow() 		# 获取顶层窗口句柄
		#dwCurID =  win32api.GetCurrentThreadId() 		# 获取当前线程 id
		arr =  win32process.GetWindowThreadProcessId(hForeWnd) 		# 获取顶层窗口线程id和进程id
		dwForeID = arr[0]		# 获取顶层窗口线程id
		
		arr =  win32process.GetWindowThreadProcessId(handle) 		# 获取传进来窗口句柄所对应的线程id和进程id
		dwAppID = arr[0]
		
		win32process.AttachThreadInput(dwForeID,dwAppID,True) 		# 将线程 dwCurID 的接收输入能力附加给线程 dwForeID，即让顶层窗口获得接受输入的能力。
		win32gui.ShowWindow(handle, win32con.SW_SHOWNORMAL)
		win32gui.SetWindowPos(handle, win32con.HWND_TOPMOST, 0,0,0,0, win32con.SWP_NOSIZE|win32con.SWP_NOMOVE)
		win32gui.SetWindowPos(handle, win32con.HWND_NOTOPMOST, 0,0,0,0, win32con.SWP_NOSIZE|win32con.SWP_NOMOVE)
		# 下面这个 SendMessage() 是阻塞的，不利于自动化，所以注释掉，用后面的 PostMessage()，它是不阻塞的，发完消息立马往下走。
		# SendMessage() 和 PostMessage() 除了向 handle 所指窗口发消息外，还有一项额外的重要功能，就是让 handle 所指的窗口（无论是否最小化）恢复到正常大小，
		# 本接口主要就是为了通过系统调用向 handle 所指的窗口发送消息使窗口恢复的功能，以便 UI 自动化能进行下去。
		#win32gui.SendMessage(handle, win32con.WM_SYSCOMMAND, win32con.SC_RESTORE, 0)
		win32gui.BringWindowToTop(handle)
		#win32gui.SetActiveWindow(handle)
		result = win32gui.SetForegroundWindow(handle) 		# 把焦点设到句柄所指窗口，即，使其成为活动窗口
		win32gui.PostMessage(handle, win32con.WM_SYSCOMMAND, win32con.SC_RESTORE, 0)
		win32process.AttachThreadInput(dwForeID,dwAppID,False) 		# 将线程 handle 从线程 dwForeID 分离出来
	except:
		print('激活窗口失败1。')
		return False
	else:
		if result is None or result == 0:
			print('激活窗口失败2。')
			return False
		else:
			return True
			
	
	
	
	
	
	
	
	
def z():
	"""
	功能说明：这个函数没有实际功能，仅仅是表明上面的公开接口到此为止，下面的函数都是内部私有函数。

	Returns
	-------
	None.

	"""
	print('这个函数没有实际功能，仅仅是表明上面的公开接口到此为止，下面的函数都是内部私有函数。')
	pass






# public API end here
# -------------------------------------------------------	
# -------------------------------------------------------	
# -------------------------------------------------------	
# -------------------------------------------------------	
# -------------------------------------------------------	
# =========================================================
# =========================================================
# =========================================================
# =========================================================
# =========================================================






# =========================================================
# =========================================================
# =========================================================
# -------------------------------------------------------	
# -------------------------------------------------------	
# -------------------------------------------------------	
# private function start here

def get_handle_arr_by_pywinauto_findwindows(class_name=None, class_name_re=None, parent=None, process=None, title=None, title_re=None, top_level_only=True, visible_only=True, enabled_only=False, best_match=None, handle=None, ctrl_index=None, found_index=None, predicate_func=None, active_only=False, control_id=None, control_type=None, auto_id=None, framework_id=None, backend=None, depth=None):
	"""
	功能说明：根据传入的参数，调用 pywinauto.findwindows.find_windows() 功能来返回相应的句柄构成的 list
		因为 pywinauto 的调用句子太长子，本接口只是对其做一层封装，其他不做任何改变。
		各参数的具体含义参见这里：https://pywinauto.readthedocs.io/en/latest/code/pywinauto.findwindows.html

	参数：
	----------
	Possible values are:

		class_name: 		Elements with this window class
		class_name_re: 		Elements whose class matches this regular expression
		parent: 			Elements that are children of this
		process: 			Elements running in this process
		title: 				Elements with this text
		title_re: 			Elements whose text matches this regular expression
		top_level_only: 	Top level elements only (default=**True**)
		visible_only: 		Visible elements only (default=**True**)
		enabled_only: 		Enabled elements only (default=False)
		best_match: 		Elements with a title similar to this
		handle: 			The handle of the element to return
		ctrl_index: 		The index of the child element to return
		found_index: 		The index of the filtered out child element to return
		predicate_func: 	A user provided hook for a custom element validation
		active_only: 		Active elements only (default=False)
		control_id: 		Elements with this control id
		control_type: 		Elements with this control type (string; for UIAutomation elements)
		auto_id: 			Elements with this automation id (for UIAutomation elements)
		framework_id: 		Elements with this framework id (for UIAutomation elements)
		backend: 			Back-end name to use while searching (default=None means current active backend)	

	返回值
	-------
		返回值：句柄构成的 list.
	"""

	handle_arr = pywinauto.findwindows.find_windows(class_name=class_name, class_name_re=class_name_re, parent=parent, process=process, title=title, title_re=title_re, top_level_only=top_level_only, visible_only=visible_only, enabled_only=enabled_only, best_match=best_match, handle=handle, ctrl_index=ctrl_index, found_index=found_index, predicate_func=predicate_func, active_only=active_only, control_id=control_id, control_type=control_type, auto_id=auto_id, framework_id=framework_id, backend=backend, depth=depth)

	return handle_arr







def get_handle_arr_by_title(title):
	"""
	功能说明：根据传入的标题 title,查找相应的 句柄号组成的 list返回

	Parameters
	----------
		title : TYPE: str
			说明：标题，根据该标题去查找句柄
	Returns
	-------
	返回值：包含 title字符串的所有句柄组成的 list

	"""
	handle_arr = []
	all_handle_arr = get_all_handle_arr()
	for handle in all_handle_arr:
		if is_my_handle(handle, title):
			handle_arr.append(handle)
			
	return handle_arr
			
	
	
	
	
	
	
def get_handle_arr_by_process_id(process_id):
	"""
	功能说明：根据传入的进程 id ,查找相应的 句柄号组成的 list返回
		注意：由于进程id 和句柄号是一对多关系，所以很可能获得的句柄很多，实际用途不大！！！
	Parameters
	----------
		process_id : TYPE: int
			说明：进程号，根据进程号去查找句柄
	Returns
	-------
	返回值：进程号所对应的的所有句柄组成的 list

	"""
	handle_arr = []
	all_handle_arr = get_all_handle_arr()
	for handle in all_handle_arr:
		if get_process_id(handle=handle) == process_id:
			handle_arr.append(handle)
			
	return handle_arr
			
	
	
	
	
	
	




def kill_app_by_name(app_name):
	"""
	功能说明：根据传入的进程名称 app_name 杀死该进程
	参数：app_name: 进程名称(只要一个程序名即可，无需全路径)
	返回值：成功返回 True, 失败返回 False
	"""
	if app_name is None:
		return False
	
	if isinstance(app_name, str):
		app_name = jxb.change_path_style(app_name)
		# 下面这一行是去掉路径，只保留程序名，因为调用 tasklist 可以看到内存中维护的进程名列表就只是一个程序名称，不含路径
		app_name = app_name.split('/')[-1]
		
	try:
		# taskkill 不能接受全路径的程序名来灭杀，因为调用 tasklist 可以看到内存中维护的进程名列表就只是一个程序名称，不含路径
		os.system('taskkill /F /IM %s /T' % (app_name))
	except:
		print('灭杀进程 %s 出错。' % (app_name))
		return False
	else:
		return True







def kill_app_by_process_id(process_id):
	"""
	功能说明：根据传入的进程ID process_id 杀死该进程
	参数：process_id: 进程 ID
	返回值：成功返回 True, 失败返回 False
	"""
	killed_flag = False 			# 是否杀掉了进程，默认值 False，表示没有
	try:
		# 先尝试调用 os.kill() 功能灭杀进程
		os.kill(process_id, signal.SIGTERM)		
	except:
		try:
			os.system('taskkill /F /PID %d /T' % (process_id))
		except:
			print('灭杀进程 %d 出错。' % (process_id))
		else:
			killed_flag = True
	else:
		killed_flag = True
		
	return killed_flag









def start_app_by_pywinauto(app_path, retry_count=5, wait_time=6):
	"""
	说明：调用 pywinauto 的 application.Application()功能来启动一个 app_path（一般是指 windows 下的 exe 程序），并返回指向这个 exe 程序的对象（句柄）到主调函数 
	参数：
		app_path, 要启动的应用程序全路径；
		retry_count: 启动失败的话最多尝试的次数；
		wait_time，启动成功后最多等待的秒数，为了充分就绪
	返回值：若启动成功，则返回指向应用程序的句柄，否则返回 None
	"""
	func_name = jxb.get_current_function_name()  		# 取得本函数名，即 start_app_by_pywinauto

	for _ in range(retry_count):
		try:
			app = application.Application().connect(path=app_path) 			# 尝试连接进程状态的应用程序
		except:
			try:
				app = application.Application().start(app_path) 		# 尝试启动应用程序
			except:
				#raise Exception('pywinauto 找不到程序 ' + app_path)
				time.sleep(1)
				continue
			else:
				time.sleep(wait_time) 	# 这个时间设长一点好，以便等待上述应用启动并准备就绪 ，8秒一般是够了，如果不够，则继续加大
				return app 		# 如果启动成功，则将 app 返回主调函数
		else:
			return app 			# 如果连接成功，则将 app 返回主调函数

	print(f'{func_name}: 已尝试 {retry_count} 次，均失败，不再尝试')
	return None 			# 如果尝试 retry_count 次后，仍失败，则返回 None 到主调函数







# private function end here
# -------------------------------------------------------	
# -------------------------------------------------------	
# -------------------------------------------------------	
# -------------------------------------------------------	
# -------------------------------------------------------	
# =========================================================
# =========================================================
# =========================================================
# =========================================================
# =========================================================



if __name__ == '__main__':

	pass
