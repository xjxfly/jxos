#coding=utf-8

"""
作者：xjxfly
邮箱：xjxfly@qq.com

说明：
这是跨平台功能接口
本模块中出现的 arr 字样，通常是指 list

"""

import jxbase as jxb


if jxb.is_windows():
	from .jxwin_lib import *

elif jxb.is_linux():
	from .jxlinux_lib import *
